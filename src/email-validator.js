const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

function validate(email) {
    const emailParts = email.split('@');
    if (emailParts.length === 2) {
        const [, domain] = emailParts;
        return VALID_EMAIL_ENDINGS.includes(domain);
    }
    return false;
}

async function validateAsync(email) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(validate(email));
        }, 100);
    });
}

function validateWithThrow(email) {
    if (!validate(email)) {
        throw new Error(`Invalid email: ${email}`);
    }
    return true;
}

function validateWithLog(email) {
    const isValid = validate(email);
    console.log(`Email: ${email}, Valid: ${isValid}`);
    return isValid;
}


module.exports = {
    validate, validateAsync, validateWithThrow, validateWithLog
}
