module.exports = {
    extends: 'airbnb-base',
    rules: {
        indent: ['error', 4],
        quotes: ['error', 'single'],
        semi: ['error', 'always'],
        'no-unused-vars': 'warn',
        'no-underscore-dangle': 'off',
    },
};
